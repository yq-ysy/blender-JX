# Blender（教学版）中文界面

#### 汉化补丁安装说明
把汉化补丁 blender.mo 文件复制到 Blender 安装目录的汉化专用目录下，例如：<br>
blender-4.2.0-beta/4.2/datafiles/locale/zh_HANS/LC_MESSAGES/<br>
覆盖替换原有的文件即可。<br>
<br>
#### Blender（教学版）中文界面——翻译原则：
（1）相关术语参考自《计算机科学技术名词》和《机械制图》国家标准，修正旧时习惯性延用的错误翻译。<br>
（2）相关词汇以“适于初学者理解”为原则，不会为了“节省字数”而生硬创造新词。<br>
（3）鼠标悬停时显示的提示框内容标注有“【教学提示】”，以介绍扩展功能和注意事项为主，不用原文。<br>
<br>
翻译工作将持续进行，逐渐完善。<br>
翻译工作量高达3万5千2百多条，共计19万1千多行文字。<br>
若有改进建议，请通过电子邮件联系： 一善鱼 <YQ-YSY@163.com> <br>
<br>
#### 快捷键 F5 快速切换“中英文界面”——设置方法：
编辑=>首选项=>快捷键=>窗口=>+新增<br>
none 填写并改选为： wm.context_toggle<br>
影响特性 填写并改选为： preferences.view.use_translate_interface<br>
点击 A 按下 F5 键。<br>
<br>
#### 快捷键 F6 快速切换鼠标悬停时显示“中英文提示框”——设置方法：
编辑=>首选项=>快捷键=>窗口=>+新增<br>
none 填写并改选为： wm.context_toggle<br>
影响特性 填写并改选为： preferences.view.use_translate_tooltips<br>
点击 A 按下 F6 键。<br>
<br>
